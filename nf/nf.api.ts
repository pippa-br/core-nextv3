import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const addAuthenticateNF = async (setting: ISetting) => 
{
    const result = await call(Types.ADD_AUTHENTICATE_NF_SERVER, setting);

    return result;
};

const updateNF = async (setting: ISetting) => 
{
    const result = await call(Types.UPDATE_NF_SERVER, setting);

    return result;
};

const issueNF = async (setting: ISetting) =>
{
    const result = await call(Types.ISSUE_NF_SERVER, setting)

    return result
}

const cancelNF = async (setting: ISetting) =>
{
    const result = await call(Types.CANCEL_NF_SERVER, setting)

    return result
}

const correctionNF = async (setting: ISetting) =>
{
    const result = await call(Types.CORRECTION_NF_SERVER, setting)

    return result
}

const updateCorrectionNF = async (setting: ISetting) =>
{
    const result = await call(Types.QUERY_NF_CORRECTION_SERVER, setting)

    return result
}

export { 
    addAuthenticateNF, 
    updateNF, 
    issueNF, 
    cancelNF,
    correctionNF,
    updateCorrectionNF, 
};
