import { usePathname } from "next/navigation";

const ClassCurrentMenu = (menu:any) =>
{
    const pathname = usePathname();

    let slug = "____";

    if (menu.type.value == "path")
    {
        slug = menu.link;
    }
    else if (menu.type.value == "collection")
    {
        slug = menu.collection.slug;
    }
    else if (menu.type.value == "category")
    {
        slug = menu.category.slug;
    }

    if (pathname.indexOf(slug) > -1)
    {
        return "selected";
    }

    return "no-selected";
}

export { ClassCurrentMenu }
