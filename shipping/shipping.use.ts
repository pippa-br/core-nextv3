import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";
import { usePathname } from "next/navigation";
 
const useCalculateFreeShipping = (setting:ISetting, onSuccess:any) =>  
{
    const pathname = usePathname();

    useEffect(() =>
    {
        call(Types.CALCULATE_FREE_SHIPPING_SERVER, setting).then((result) => 
        {			
            onSuccess(result.data);
        });	
    }, [ pathname ]);
} 

export { 
    useCalculateFreeShipping,
}