import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { Matrix } from "../model/matrix";
 
const calculateShipping = async (setting:ISetting) => 
{
    const result = await call(Types.CALCULATE_SHIPPING_SERVER, setting);
	
    return result;
}

const getTrackCorreios = async (setting:ISetting) => 
{
    const result = await call(Types.GET_TRACK_CORREIOS_SERVER, setting);
	
    return result;
}

const getAddDays = (product:any, listVariant:any) =>
{
    if (product)
    {
        if (product.addDaysTable)
        {
            const stockTable = new Matrix(product.addDaysTable);
            const data       = stockTable.getValueByListVariant(listVariant);
	
            for (const key in data)
            {
                if (data && data[key].days > 0)
                {
                    return data[key].days;
                }
            }
        }	
        else if (product.addDays > 0)
        {
            return product.addDays;
        }	
    }

    return 0;
}

export { 
    calculateShipping, 
    getTrackCorreios, 
    getAddDays
}