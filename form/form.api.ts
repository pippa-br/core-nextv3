import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";


const getForm = async (setting: ISetting) => 
{
    const result = await call(Types.GET_FORM_SERVER, setting);

    return result;
}


const collectionForm = async (setting: ISetting) => 
{
    const result = await call(Types.COLLECTION_FORM_SERVER, setting);

    return result;
}


export {
    getForm,
    collectionForm
}