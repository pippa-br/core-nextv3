import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const printOrder = async (setting: ISetting) => 
{
    const result = await call(Types.PRINT_ORDER_SERVER, setting);

    return result;
};

const printReport = async (setting: ISetting) => 
{
    const result = await call(Types.PRINT_REPORT_SERVER, setting);
    return result;
}

const printBarcodesTable = async (setting: ISetting) =>
{
    const result = await call(Types.PRINT_BARCODES_TABLE_SERVER, setting);
    return result;
}

export { printOrder, printReport, printBarcodesTable };
