import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";

const useGetOrder = (setting:ISetting, onSuccess:any) =>  
{
    useEffect(() => 
    {
        call(Types.GET_ORDER_SERVER, setting).then((result) => 
        {
            onSuccess(result.data);
        });
        
    }, []);	
}

export { 
    useGetOrder
};
