import { TDate } from "../model/TDate";

const validateReorder = (order:any, addDays: any) =>
{	
    if (order.openReorder)
    {
        return !order.hasReorder;
        //return order.statusPayment == 'Pago' && order.hasNF && !order.hasReorder;
    }
    else if ((order.postdate || order.deliveredDate) && addDays > 0)
    {
        const dateStart = new TDate();
        const dateEnd   = new TDate({ value : order.deliveredDate || order.postdate }).add(addDays, "days");
        const days      = dateEnd.diff(dateStart, "days");

        //console.error('xxx', order);

        //console.log('validateReorder', order, days, order.statusPayment == 'Pago' && order.hasNF && !order.hasReorder && days >= 0)

        return order.statusPayment == "Pago" &&  
					 (
					     order?.status?.value == "nf" || 
						order?.status?.value == "separation" || 
						order?.status?.value == "charged" || 
						order?.status?.value == "delivered" ||
						order?.status?.value == "posted"
					 ) && !order.hasReorder && days >= 0;
    }	

    return false;
}

const orderStatusLabel = (order:any) =>
{
    const labes : any = {
        generated      	: "Aguardando",
        waiting_payment : "Aguardando",
        pending        	: "Aguardando",
        not_authorized  : "Não Autorizado",
        captured       	: "Pago",
        failed          : "Não Autorizado",
    }

    if (order.statusPayment == "Aguardando")
    {
        return labes[order.chargeStatus] || (!order.chargeStatus ? "Aguardando" : order.chargeStatus);
    }
    else
    {
        return order.statusPayment;
    }	
}


const orderStatusClass = (order:any) =>
{
    if (order.statusPayment == "Aguardando")
    {
        return order.chargeStatus;
    }
    else if (order.statusPayment == "Cancelado")
    {
        return "not_authorized";
    }
    else if (order.statusPayment == "Recusado")
    {
        return "not_authorized";
    }	
}


const orderStatusBarActive = (trackBackItem:any, order: any) =>
{	
    const statesOrder : any = {
        new : {
            new : "currentStatus",
        },
        canceled : {
            new  : "activeStatus",
            paid : "activeStatusCanceled",
        },
        paid : {
            new  : "activeStatus",
            paid : "currentStatus",
        },
        nf : {
            new  : "activeStatus",
            paid : "activeStatus",
            nf 	 : "currentStatus",
        },
        available : {
            new    	  : "activeStatus",
            paid   	  : "activeStatus",
            nf   	 	  : "activeStatus",
            available : "currentStatus",
            posted    : "currentStatus",
        },
        posted : {
            new       : "activeStatus",
            paid      : "activeStatus",
            // nf 	      : 'activeStatus',
            nf 	      : "currentStatus",
            available : "activeStatus", 
            posted    : "currentStatus",
        },
        movement : {
            new       : "activeStatus",
            paid      : "activeStatus",
            nf  	     : "activeStatus",
            posted    : "activeStatus",
            available : "currentStatus",
            movement  : "currentStatus",
        },
        delivered : {
            new       : "activeStatus",
            paid      : "activeStatus",
            nf 	      : "activeStatus",
            posted    : "activeStatus",
            movement  : "activeStatus",
            delivered : "currentStatus",
        }		
    }

    if (statesOrder[order.status.value])
    {
        return statesOrder[order.status.value][trackBackItem.status.value];
    }
}

export { 
    validateReorder,
    orderStatusLabel,
    orderStatusClass,
    orderStatusBarActive
}