import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const collectionOrder = async (setting: ISetting) => 
{
    const result = await call(Types.COLLECTION_ORDER_SERVER, setting);

    return result;
};

const getOrder = async (setting: ISetting) => 
{
    const result = await call(Types.GET_ORDER_SERVER, setting);

    return result;
};
 
const addPaymentOrder = async (setting:ISetting) => 
{
    const result = await call(Types.ADD_PAYMENT_ORDER_SERVER, setting);
	
    return result;
}

const addReorder = async (setting:ISetting) => 
{
    const result = await call(Types.ADD_REORDER_SERVER, setting);
	
    return result;
}

const setStatusByStoreOrder = async (setting: ISetting) => 
{
    const result = await call(Types.SET_STATUS_BY_STORE_ORDER_SERVER, setting);

    return result;
};

const setPaymentMethodOrder = async (setting: ISetting) => 
{
    const result = await call(Types.SET_PAYMENT_METHOD_ORDER_SERVER, setting);

    return result;
};

const setInstallmentOrder = async (setting: ISetting) => 
{
    const result = await call(Types.SET_INSTALLMENT_ORDER_SERVER, setting);

    return result;
};

const setCreditCardOrder = async (setting: ISetting) => 
{
    const result = await call(Types.SET_CREDIT_CARD_ORDER_SERVER, setting);

    return result;
};

const setCreditCardReferenceOrder = async (setting: ISetting) => 
{
    const result = await call(Types.SET_CREDIT_CARD_REFERENCE_ORDER_SERVER, setting);

    return result;
};

const splitByStoreOrder = async (setting: ISetting) => 
{
    const result = await call(Types.SPLIT_BY_STORE_ORDER_SERVER, setting);

    return result;
};

const setItemOrder = async (setting: ISetting) => 
{
    const result = await call(Types.SET_ITEM_ORDER_SERVER, setting);

    return result;
};

const approvedOrder = async (setting: ISetting) => 
{
    const result = await call(Types.APPROVED_ORDER_SERVER, setting);

    return result;
};

const canceledOrder = async (setting: ISetting) => 
{
    const result = await call(Types.CANCELED_ORDER_SERVER, setting);

    return result;
};

const setCartOrder = async (setting: ISetting) => 
{
    const result = await call(Types.UPDATE_CART_ORDER_SERVER, setting);
  
    return result;
}

const closurePDV = async (setting: ISetting) =>
{
    const result = await call(Types.CLOSURE_PDV_SERVER, setting)

    return result
}

const newStatusOrder = async (setting: ISetting) =>
{
    const result = await call(Types.NEW_STATUS_ORDER_SERVER, setting)

    return result
}

const awaitingStatusOrder = async (setting: ISetting) =>
{
    const result = await call(Types.AWAITING_STATUS_ORDER_SERVER, setting)

    return result
}

const paidStatusOrder = async (setting: ISetting) =>
{
    const result = await call(Types.PAID_STATUS_ORDER_SERVER, setting)

    return result
}

const setClientOrder = async (setting: ISetting) =>
{
    const result = await call(Types.SET_CLIENT_ORDER_SERVER, setting)

    return result
}

const setPaymentStatusOrder = async (setting: ISetting) =>
{
    const result = await call(Types.SET_PAYMENT_STATUS_ORDER_SERVER, setting)

    return result
}

const cartToOrder = async (setting: ISetting) =>
{
    const result = await call(Types.CART_TO_ORDER_SERVER, setting)

    return result
}

const setInstallmentMethodsOrder = async (setting: ISetting) =>
{
    const result = await call(Types.SET_INSTALLMENT_METHODS_ORDER_SERVER, setting)

    return result
}

const setSellerOrder = async (setting: ISetting) =>
{
    const result = await call(Types.SET_SELLER_ORDER_SERVER, setting)

    return result
}

const paymentsPDV = async (setting: ISetting) =>
{
    const result = await call(Types.PAYMENTS_PDV_SERVER, setting)

    return result
}

const sellersPDV = async (setting: ISetting) => 
{
    const result = await call(Types.SELLERS_PDV_SERVER, setting)

    return result
}

const totalOrdersPDV = async (setting: ISetting) =>
{
    const result = await call(Types.TOTAL_ORDERS_PDV_SERVER, setting)

    return result
}

const paidOrdersPDV = async (setting: ISetting) =>
{
    const result = await call(Types.PAID_ORDERS_PDV_SERVER, setting)

    return result
}

const agentOrdersPDV = async (setting: ISetting) =>
{
    const result = await call(Types.AGENT_ORDERS_PDV_SERVER, setting)

    return result
}

export { 
    collectionOrder, 
    getOrder, 
    setStatusByStoreOrder, 
    splitByStoreOrder, 
    setItemOrder, 
    approvedOrder, 
    canceledOrder, 
    setPaymentMethodOrder, 
    setCreditCardOrder, 
    setInstallmentOrder, 
    setCreditCardReferenceOrder,
    addPaymentOrder,
    addReorder,
    setCartOrder,
    closurePDV,
    newStatusOrder,
    awaitingStatusOrder,
    paidStatusOrder,
    setClientOrder,
    setPaymentStatusOrder,
    cartToOrder,
    setInstallmentMethodsOrder,
    setSellerOrder,
    paymentsPDV,
    sellersPDV,
    totalOrdersPDV,
    paidOrdersPDV,
    agentOrdersPDV,
};
