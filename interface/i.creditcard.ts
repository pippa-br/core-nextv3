export interface CreditCardProps {
  cardnumber: string;
  cvv: string;
  owner: string;
  address: {
    state: string;
    district: string;
    country: {
      value: "br";
      id: "br";
      label: "Brasil";
    };
    complement: string;
    housenumber: string;
    zipcode: string;
    city: string;
    street: string;
  };
  _cpf: string;
  docType: {
    type: string;
    id: string;
    label: string;
    value: string;
  };
  expirydate: string;
}

export interface CreditCardsProps {
  creditCard: CreditCardProps;
  referencePath: string;
  id: string;
}
