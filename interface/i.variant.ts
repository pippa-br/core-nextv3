export default interface VariantProps {
    id: string;
    label: string;
    value: string;
  }
  