export default interface IUser {
  name: string;
  email: string;
  cpf: string;
  phone: string;
  points:number;
  referencePath: string;  
  address:any;
}

export interface UserProps {
  name: string;
  store: {
    name: string;
    code: string;
    id: string;
    referencePath: string;
    appid: string;
    accid: string;
  };
  type: string;
  cpf: string;
  cnpj: string;
  referencePath: string;
}
