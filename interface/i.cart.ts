export default interface ICart 
{
    referencePath : string;
    items: any;
    paymentMethod: any;
    creditCard: any;
    coupon: any;
    address: any;
    shipping: any;
    installment:any;
    totalItems: number;
    totalDiscount: number;
    totalInterest:number;
    totalCreditDiscount:number;
    creditClient: number;
    discountClient:number;
    total: number;
    store : {
        accid: string;
        appid: string;
        code: string;
        id: string;
        logo: any;
        name: string;
        referencePath: string;
        phone: string;
    };
    client: {
        accid: string;
        appid: string;
        id: string;
        name: string;
        referencePath: string;
    };
}
