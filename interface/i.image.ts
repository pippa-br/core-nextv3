export interface ImageSetProps {
  image: {
    id: string;
    name: string;
    _url: string;
    _300x300: string;
    _150x150: string;
    _1024x1024: string;
  };
}
