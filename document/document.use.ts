import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";
import { usePathname } from "next/navigation";

const useCollectionDocument = (setting:ISetting, onSuccess?:any) => 
{
    const pathname = usePathname();
    
    useEffect(() => 
    {
        call(Types.COLLECTION_DOCUMENT_SERVER, setting).then((result) => 
        {
            onSuccess(result.collection || [])
        });	
    }, [ pathname ]);
}

const useGetDocument = (setting:ISetting, onSuccess?:any) => 
{	
    useEffect(() => 
    {
        call(Types.GET_DOCUMENT_SERVER, setting).then((result) => 
        {
            if (onSuccess)
            {
                onSuccess(result.data)
            }			
        });	
    }, []);
}

export {
    useCollectionDocument,
    useGetDocument,
}