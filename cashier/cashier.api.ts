import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useRouter } from "next/router";
import { useEffect } from "react";

const useGetLastOpenCashier = (setting:ISetting, onSuccess: any) => 
{
    const router = useRouter();

    useEffect(() => 
    {
        call(Types.LAST_CASHIER_SERVER, setting).then((result: any) =>
        {
            if (result.data)
            {
                onSuccess(result.data);
            }
            else
            {
                onSuccess(null);
            }
        });
    }, [ router.asPath ]);	
}

const getLastOpenCashier = async (setting:ISetting) => 
{
    const result = await call(Types.LAST_CASHIER_SERVER, setting);
	
    return result;
}

const cashierCollection = async (setting:ISetting) => 
{
    const result = await call(Types.CASHIER_COLLECTION_SERVER, setting);
	
    return result;
}

const addCashierMovement = async (setting:ISetting) => 
{
    const result = await call(Types.CASHIER_MOVEMENT_ADD_SERVER, setting);
	
    return result;
}

export { 
    cashierCollection, addCashierMovement, useGetLastOpenCashier, getLastOpenCashier
}