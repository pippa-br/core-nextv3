import { calls } from "@/core-nextv3/util/call.api";
import { PARTIAL_PRODUCT_VARIANT_SETTING } from "@/setting/setting";
import { useState } from "react";
import { searchProductVariant } from "../product/product.api";

export function useSearchProducts({ 
    onLoadProducts,
    settingVariants = PARTIAL_PRODUCT_VARIANT_SETTING
}:any) 
{
    const [ loading, setLoading ] = useState(false);
    const [ items, setItems ] = useState<any[]>([]);
    const [ totalItems, setTotalItems ] = useState<number>(0);
    const [ hasNextPage, setHasNextPage ] = useState<boolean>(true);
    const [ error, setError ] = useState<Error>();

    async function loadProducts(filter:any) 
    {
        setLoading(true);

        try 
        {
            const params : any = {
                page    : filter.page,
                orderBy : filter.orderBy,
                asc     : filter.asc,
                perPage : filter.perPage,
                term    : filter.term,
            };
            
            const [ result ] = await calls(
                searchProductVariant(settingVariants.merge(params)),
            );

            setTotalItems(result.total);

            if (result.collection && result.collection.length > 0)
            {
                if (filter.merge)
                {
                    setItems((prevItems) => [ ...prevItems, ...result.collection ]);            
                }
                else
                {
                    setItems(result.collection);
                }
            
                setHasNextPage(filter.perPage == result.count);

                if (onLoadProducts)
                {
                    onLoadProducts(result.collection)
                }
            }
            else
            {
                if (!filter.merge)
                {
                    setItems([]);
                }

                setHasNextPage(false);
            }	

            setLoading(false);
        }
        catch (error_) 
        {
            setError(
                error_ instanceof Error ? error_ : new Error("Something went wrong"),
            );
        }
        finally 
        {
            setLoading(false);
        }
    }

    return { loading, items, hasNextPage, error, loadProducts, totalItems };
}