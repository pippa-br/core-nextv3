import { countDocument } from "@/core-nextv3/document/document.api";
import { collectionProductVariant } from "@/core-nextv3/product/product.api";
import { productParseQuery } from "@/core-nextv3/product/product.util";
import { calls } from "@/core-nextv3/util/call.api";
import { PARTIAL_PRODUCT_VARIANT_SETTING, platform, THEME_SETTING } from "@/setting/setting";
import { useState } from "react";

export function useListProducts({ 
    colorVariantsData, 
    sizeVariantsData, 
    category, 
    levelVariant1, 
    onLoadProducts, 
    brands,
    settingVariants = PARTIAL_PRODUCT_VARIANT_SETTING
}:any) 
{
    const [ init, setInit ] = useState(false);
    const [ loading, setLoading ] = useState(false);
    const [ items, setItems ] = useState<any[]>([]);
    const [ totalItems, setTotalItems ] = useState<number>(0);
    const [ hasNextPage, setHasNextPage ] = useState<boolean>(false);
    const [ error, setError ] = useState<Error>();

    async function loadProducts(filter:any) 
    {
        setInit(true);
        setLoading(true);

        try 
        {
            const params : any = {
                page          : filter.page,
                orderBy       : filter.orderBy,
                asc           : filter.asc,
                perPage       : filter.perPage,
                levelVariant1 : levelVariant1,
                searchVector  : filter.term,
                where         : [
                    {
                        field    : "published",
                        operator : "==",
                        value    : true,
                    },
                    ...productParseQuery(filter, {
                        categories : category  ? [ category ] : [],
                        colors     : colorVariantsData,
                        sizes      : sizeVariantsData,
                        brands     : brands,
                    }),
                    ...filter.where ],
            };

            if (THEME_SETTING.filterPlatform)
            {
                params.where.push({
                    field    : "indexes.platforms." + platform.value,
                    operator : "==",
                    value    : true, 
                })
            }

            const [ result, resultTotal ] = await calls(
                collectionProductVariant(settingVariants.merge(params)),
                countDocument(settingVariants.merge(params))
            );

            setTotalItems(resultTotal.total);

            if (result.collection && result.collection.length > 0)
            {
                if (filter.merge)
                {
                    setItems((prevItems) => [ ...prevItems, ...result.collection ]);            
                }
                else
                {
                    setItems(result.collection);
                }
            
                setHasNextPage(filter.perPage == result.total);

                if (onLoadProducts)
                {
                    onLoadProducts(result.collection)
                }
            }
            else
            {
                if (!filter.merge)
                {
                    setItems([]);
                }

                setHasNextPage(false);
            }

            setLoading(false);
        }
        catch (error_) 
        {
            setError(
                error_ instanceof Error ? error_ : new Error("Something went wrong"),
            );
        }
        finally 
        {
            setLoading(false);
        }
    }

    return { loading, items, hasNextPage, error, loadProducts, totalItems, init };
}