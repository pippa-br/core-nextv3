import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const addPointsByReorderTotal = async (setting:ISetting) => 
{
    const result = await call(Types.ADD_POINTS_BY_REORDER_TOTAL_SERVER, setting);
    return result;
}

const addPointsByDocument = async (setting:ISetting) => 
{
    const result = await call(Types.ADD_POINTS_BY_DOCUMENT, setting);
    return result;
}

export {
    addPointsByReorderTotal,
    addPointsByDocument
}
