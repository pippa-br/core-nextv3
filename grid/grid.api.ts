import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const getGrid = async (setting: ISetting) => 
{
    const result = await call(Types.GET_GRID_SERVER, setting);

    return result;
}

export { getGrid }