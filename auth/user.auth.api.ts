"use client"

import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import { useCore } from "../core/core";
import { useCustomPush } from "../util/util.use";

const useGetLoggedAuth = (setting:ISetting, onSuccess:any) => 
{
    const { setIsLoadingUser } = useCore();

    useEffect(() => 
    {
        setIsLoadingUser(false);

        call(Types.GET_LOGGED_AUTH_SERVER, setting).then(result => 
        {
            onSuccess(result.data);

            setIsLoadingUser(true);
        });
    }, []);	
}

const useGetAuthToken = (setting:ISetting, onSuccess:any) => 
{
    const { setIsLoadingUser } = useCore();

    useEffect(() => 
    {
        setIsLoadingUser(false);

        call(Types.GET_AUTH_TOKEN_SERVER, setting).then(result => 
        {
            onSuccess(result.data);

            setIsLoadingUser(true);
        });
    }, []);	
}

const useGetUserAuth = (setting:ISetting, onSuccess:any) => 
{
    const { setIsLoadingUser } = useCore();

    useEffect(() => 
    {
        setIsLoadingUser(false);

        call(Types.GET_USER_AUHT_SERVER, setting).then(result => 
        {
            onSuccess(result.data)

            setIsLoadingUser(true);
        });
    }, []);	
}

const useProtectedAuth = (setting:ISetting, successUrl?:string, failUrl?:string) => 
{
    const router                                      = useRouter();
    const customPush                                  = useCustomPush();
    const [ loadProtectedAuth, setPoadProtectedAuth ] = useState(false);	
    const { user, isLoadingUser } = useCore();

    useEffect(() => 
    {
        if (isLoadingUser)
        {
            if (user)
            {
                if (successUrl && successUrl == "back")
                {
                    router.back();
                }
                else if (successUrl)
                {
                    customPush(successUrl);
                }
                else
                {
                    setPoadProtectedAuth(true);
                }				
            }
            else
            {
                if (failUrl)
                {
                    customPush(failUrl);
                }
                else
                {
                    setPoadProtectedAuth(true);
                }				
            }
        }		
    }, [ isLoadingUser ])

    return loadProtectedAuth;
}

export { 
    useProtectedAuth,
    useGetUserAuth, 
    useGetLoggedAuth, 
    useGetAuthToken,
}