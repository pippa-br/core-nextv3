"use client";

import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";
import { usePathname } from "next/navigation";

const useCollectionProductVariantFavorite = (setting:ISetting, onSuccess?:any) => 
{
    const pathname = usePathname();
	
    useEffect(() => 
    {
        call(Types.COLLECTION_PRODUCT_VARIANT_FAVORITE_SERVER, setting).then((result) => 
        {
            onSuccess(result.collection || [])
        });	
    }, [ pathname ]);
}

const useCollectionFavorite = (setting:ISetting, onSuccess?:any) => 
{
    const pathname = usePathname();
	
    useEffect(() => 
    {
        call(Types.COLLECTION_FAVORITE_SERVER, setting).then((result) => 
        {
            onSuccess(result.collection || [])
        });	
    }, [ pathname ]);
}

const useGetFavorite = (setting:ISetting, onSuccess?:any) => 
{
    const pathname = usePathname();
	
    useEffect(() => 
    {
        call(Types.GET_FAVORITE_SERVER, setting).then((result) => 
        {
            onSuccess(result.status)
        });	
    }, [ pathname ]);
}

export { 
    useCollectionProductVariantFavorite, 
    useCollectionFavorite,
    useGetFavorite, 
}