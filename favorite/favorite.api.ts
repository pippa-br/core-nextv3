import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const collectionProductVariantFavorite = async (setting:ISetting) => 
{
    const result = await call(Types.COLLECTION_PRODUCT_VARIANT_FAVORITE_SERVER, setting);
	
    return result;
}

const setFavorite = async (setting:ISetting) => 
{
    const result = await call(Types.SET_FAVORITE_SERVER, setting);
	
    return result;
}

export { 
    collectionProductVariantFavorite,
    setFavorite 
}