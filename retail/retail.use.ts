"use client"

import Types from "../type";
import { useEffect } from "react";
import { call } from "../util/call.api";
import { ACCOUNT_SETTING, CART_SETTING } from "@/setting/setting";
import { useSearchParams } from "next/navigation";

const useDetailPageViewRetail = (setting:any) => 
{
    const searchParams = useSearchParams();
    
    useEffect(() => 
    {
        call(Types.DETAIL_PAGE_VIEW_RETAIL_SERVER, setting);

    }, [ searchParams ]);	
}

const useHomePageViewRetail = () => 
{
    useEffect(() => 
    {
        call(Types.HOME_PAGE_VIEW_RETAIL_SERVER, ACCOUNT_SETTING);
      
    }, []);
};

const useCategoryPageViewRetail = (categoryData:any) => 
{
    useEffect(() => 
    {
        call(Types.CATEGORY_PAGE_VIEW_RETAIL_SERVER, ACCOUNT_SETTING.merge({
            categories : [ categoryData?.name ]
        }));
      
    }, []);
};

const useShoppingCartPageViewRetail = () => 
{
    useEffect(() => 
    {
        call(Types.SHOPPING_CART_PAGE_VIEW_RETAIL_SERVER, CART_SETTING);
      
    }, []);
};

export { 
    useDetailPageViewRetail, 
    useHomePageViewRetail,	
    useCategoryPageViewRetail,
    useShoppingCartPageViewRetail,
}