import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const addToCartRetail = async (setting:ISetting) => 
{
    const result = await call(Types.ADD_TO_CART_RETAIL_SERVER, setting);
	
    return result;
}

const purchaseCompleteRetail = async (setting:ISetting) => 
{
    const result = await call(Types.PURCHASE_COMPLETE_RETAIL_SERVER, setting);
	
    return result;
}

export { 
    addToCartRetail, 
    purchaseCompleteRetail,
}