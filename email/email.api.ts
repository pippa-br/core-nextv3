import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const sendTransection = async (setting: ISetting) => 
{
    const result = await call(Types.SEND_TRANSECTION_SERVER, setting);

    return result;
};

export { sendTransection };
