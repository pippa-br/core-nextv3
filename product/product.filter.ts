import createFilter from "../util/createFilter";

const useProductFilter = createFilter({
    page    : 1,
    perPage : 24,
    color   : null,
    size    : null,
    orderBy : "order",
    asc     : false,
    where   : [],
    term    : "",
    merge   : false,
    brand   : null, 
});

export default useProductFilter;