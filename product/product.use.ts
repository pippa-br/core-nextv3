import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";
import { usePathname } from "next/navigation";

const useCollectionProductVariant = (setting:ISetting, onSuccess:any) => 
{
    const pathname = usePathname();

    useEffect(() => 
    {
        call(Types.COLLECTION_PRODUCT_VARIANT_SERVER, setting).then(result => 
        {
            onSuccess(result.collection)
        });
    }, [ pathname ]);
}

const useGetStockTableProduct = (setting:ISetting, onSuccess:any) => 
{
    const pathname = usePathname();

    useEffect(() => 
    {
        call(Types.GET_STOCK_TABLE_SERVER, setting).then(result => 
        {
            onSuccess(result.data)
        });
    }, [ pathname ]);	
}

export { 
    useGetStockTableProduct,	
    useCollectionProductVariant
}