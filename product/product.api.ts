import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const collectionProduct = async (setting:ISetting) => 
{
    const result = await call(Types.COLLECTION_PRODUCT_SERVER, setting);
	
    return result;
}

const collectionProductVariant = async (setting:ISetting) => 
{
    const result = await call(Types.COLLECTION_PRODUCT_VARIANT_SERVER, setting);
	
    return result;
}

const searchProductVariant = async (setting:ISetting) => 
{
    const result = await call(Types.SEARCH_PRODUCT_VARIANT_SERVER, setting);
	
    return result;
}

const collectionProductVariantByCollection = async (setting:ISetting) => 
{
    const result = await call(Types.COLLECTION_PRODUCT_VARIANT_BY_COLLECTION_SERVER, setting);
	
    return result;
}

const getProduct = async (setting:ISetting) => 
{
    const result = await call(Types.GET_PRODUCT_SERVER, setting);
	 
    return result;
}

const notificationProduct = async (setting:ISetting) => 
{
    const result = await call(Types.NOTIFICATION_PRODUCT_SERVER, setting);
    
    return result;
}

export { 
    collectionProduct, 
    collectionProductVariant,
    collectionProductVariantByCollection,
    searchProductVariant,
    getProduct, 
    notificationProduct,
}