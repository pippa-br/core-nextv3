import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const getLessonsByUser = async (setting: ISetting) => 
{
    const result = await call(Types.GET_LESSON_BY_USER_SERVER, setting);

    return result;
};

export { getLessonsByUser };
