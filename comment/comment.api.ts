import Types 	    from "../type";
import { ISetting } from "../util/setting";
import { call } 	from "../util/call.api";

const addComment = async (setting:ISetting) => 
{
    const result = await call(Types.ADD_COMMENT_SERVER, setting);
	
    return result;
}

export { 
    addComment 
}