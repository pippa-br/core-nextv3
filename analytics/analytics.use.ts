"use client"

import Types from "../type";
import { useSearchParams } from "next/navigation";
import { useEffect } from "react";
import { browserName, isDesktop, osName,  } from "react-device-detect";
import { getDomainFromReferrer } from "../util/util";
import { todayWeather } from "../weather/weather.api";
import { TDate } from "../model/TDate";
import { useCookies } from "react-cookie"
import { registerEvent } from "./analytics.api";

// UNIQUE USER
const useUniqueUserAnalytics = () => 
{
    const [ cookies, setCookie ] = useCookies([ "uniqueUserAnalytics" ]);

    useEffect(() => 
    {
        const expires = new Date()
    	expires.setTime(expires.getTime() + (30 * 60 * 1000));

        if (cookies.uniqueUserAnalytics != "close") 
        {
            registerEvent({
                action : Types.UNIQUE_USER_ACTION_ANALYTICS,
                keys   : [ "Usuário Único" ],
            });
        }

        setCookie("uniqueUserAnalytics", "close", { path : "/", expires });

    }, []);	
}

// DEVICE
const useDeviceAnalytics = () => 
{
    const [ cookies, setCookie ] = useCookies([ "deviceAnalytics" ]);

    useEffect(() => 
    {
        const expires = new Date()
    	expires.setTime(expires.getTime() + (30 * 60 * 1000))

        if (cookies.deviceAnalytics != "close") 
        {
            registerEvent({
                action : Types.DEVICE_ACTION_ANALYTICS,
                keys   : [ (isDesktop ? "Desktop" : "Mobile"), osName, browserName ],
            });			
        }

        setCookie("deviceAnalytics", "close", { path : "/", expires });

    }, []);	
}

// REFERRER
const useReferrerAnalytics = () => 
{
    const [ cookies, setCookie ] = useCookies([ "referrerAnalytics" ]);

    useEffect(() => 
    {
        const expires = new Date()
    	expires.setTime(expires.getTime() + (30 * 60 * 1000))

        if (cookies.referrerAnalytics != "close") 
        {
            const domain = getDomainFromReferrer();

            registerEvent({
                action : Types.REFERRER_ACTION_ANALYTICS,
                keys   : [ domain ],
            });			
        }

        setCookie("referrerAnalytics", "close", { path : "/", expires });

    }, []);	
}

// PERIODO DO DIA
const usePeriodsDaysAnalytics = () => 
{
    const [ cookies, setCookie ] = useCookies([ "periodsDaysAnalytics" ]);
	
    useEffect(() => 
    {
        const expires = new Date()
    	expires.setTime(expires.getTime() + (30 * 60 * 1000))

        if (cookies.periodsDaysAnalytics != "close") 
        {			
            const dayWeek  = new TDate().format("dddd");
            const hour     = parseInt(new TDate().format("HH"));
            let label      = "";
	
            if (hour >= 0 && hour < 6)
            {
                label = "Madrugada";
            }
            else if (hour >= 6 && hour < 12)
            {
                label = "Manahã";
            }
            else if (hour >= 12 && hour < 18)
            {
                label = "Tarde";
            }
            else if (hour >= 18 && hour < 24)
            {
                label = "Noite";
            }
	
            registerEvent({
                action : Types.PERIODS_DAYS_ACTION_ANALYTICS,
                keys   : [ label ],
            });

            registerEvent({
                action : Types.HOUR_DAY_ACTION_ANALYTICS,
                keys   : [ hour ],
            });

            registerEvent({
                action : Types.DAY_WEEK_ACTION_ANALYTICS,
                keys   : [ dayWeek ],
            });
        }

        setCookie("periodsDaysAnalytics", "close", { path : "/", expires });

    }, []);	
}

// CLIMA TEMPO
const useWeatherAnalytics = () => 
{
    const [ cookies, setCookie ] = useCookies([ "weatherAnalytics" ]);

    useEffect(() => 
    {
        const expires = new Date()
        expires.setTime(expires.getTime() + (30 * 60 * 1000))

        if (cookies.weatherAnalytics != "close") 
        {
            const call = async () =>
            {
                const result = await todayWeather();
	
                if (result.data && result.data.daily && result.data.daily.weather)
                {
                    for (const item of result.data.daily.weather)
                    {
                        registerEvent({
                            action : Types.WEATHER_ACTION_ANALYTICS,
                            keys   : [ item.label ],
                        });
                    }
                }							
            }		
	
            call();	
        }

        setCookie("weatherAnalytics", "close", { path : "/", expires });

    }, []);	
}

// QUANDO O PRODUTO VEM DO BACK
const useProductClickAnalytics = (product:any) => 
{
    const searchParams = useSearchParams()
    const color = searchParams.get("color");
    const size  = searchParams.get("size");
	
    // COLOR VARIANTES 
    useEffect(() => 
    {
        if (color)
        {
            registerEvent({
                action   : Types.VARIANT_CLICK_ACTION_ANALYTICS,
                keys     : [ "level-1: " + color ],
                document : {
                    referencePath : product.referencePath
                }
            });			
        }

    }, [ color ]);

    // SIZE VARIANTES 
    useEffect(() => 
    {
        if (size)
        {
            registerEvent({
                action   : Types.VARIANT_CLICK_ACTION_ANALYTICS,
                keys     : [ "level-2: " + size ],
                document : {
                    referencePath : product.referencePath
                }
            });
        }

    }, [ size ]);


    // ALL VARIANTES E SKU
    // useEffect(() => 
    // {
    // 	if(query.color && query.size)
    // 	{
    // 		// ALL
    // 		registerEvent({
    // 			action   : Types.VARIANT_CLICK_ACTION_ANALYTICS,
    // 			keys     : ['all: ' + query.color + '-' + query.size],
    // 			document : {
    // 				referencePath : product.referencePath
    // 			}
    // 		});

    // 		// SKU
    // 		registerEvent({
    // 			action   : Types.SKU_CLICK_ACTION_ANALYTICS,
    // 			keys     : [product.code + '-' + query.color + '-' + query.size],
    // 			document : {
    // 				name : product.name,
    // 				code : product.code,
    // 				document : {
    // 					referencePath : product.referencePath
    // 				}
    // 			}
    // 		});
    // 	}

    // }, [query.color, query.size]);

    // PRODUTOS
    useEffect(() => 
    {
        registerEvent({
            action   : Types.PRODUCT_CLICK_ACTION_ANALYTICS,
            keys     : [ product.code + ": " + product.name ],
            document : {
                referencePath : product.referencePath
            }
        });

    }, []);	
}

// QUANDO A CATEGORIA VEM DO BACK
const useCategoryClickAnalytics = (category:any) => 
{
    useEffect(() => 
    {
        if (category)
        {
            registerEvent({
                action   : Types.CATEGORY_CLICK_ACTION_ANALYTICS,
                keys     : [ category.name ],
                document : {
                    referencePath : category.referencePath
                }
            });	
        }

    }, []);	
}

const useCollectionClickAnalytics = (page:any) => 
{
    // PRODUTOS
    useEffect(() => 
    {
        registerEvent({
            action   : Types.COLLECTION_CLICK_ACTION_ANALYTICS,
            keys     : [ page?.name ],
            document : {
                referencePath : page?.referencePath
            }
        });	

    }, []);	
}

export { 
    useProductClickAnalytics, 	
    useCategoryClickAnalytics,
    useCollectionClickAnalytics,
    usePeriodsDaysAnalytics,
    useWeatherAnalytics,
    useDeviceAnalytics,
    useReferrerAnalytics,
    useUniqueUserAnalytics,
}