import Types from "../type";
import { call } from "../util/call.api";
import { ANALYTICS_SETTING } from "@/setting/setting";
class AnalyticsEvents 
{
    static events : any = [];
    static pid : any;
}

const registerEvent = (event:any, timeout = 2000) => 
{
    if (!AnalyticsEvents.events) 
    {
	  	AnalyticsEvents.events = [];
    }

    // // REMOVE DUPLICATES DOCUMENTS
    // if(event.documents)
    // {
    // 	const data 		: any = {};
    // 	const documents = []

    // 	for(const item of event.documents)
    // 	{
    // 		if(!data[item.referencePath])
    // 		{
    // 			documents.push(item);
    // 			data[item.referencePath] = true;
    // 		}
    // 	}

    // 	event.documents = documents;
    // }

    // // REMOVE DUPLICATE KEYS
    // if(event.keys)
    // {
    // 	const data : any = {};
    // 	const keys = []

    // 	for(const name of event.keys)
    // 	{
    // 		if(!data[name])
    // 		{
    // 			keys.push(name);
    // 			data[name] = true;
    // 		}
    // 	}

    // 	event.keys = keys;
    // }

    AnalyticsEvents.events.push(event);

    if (AnalyticsEvents.pid)
    {
        clearTimeout(AnalyticsEvents.pid);
    }

    AnalyticsEvents.pid = setTimeout(async () => 
    {		
        if (ANALYTICS_SETTING.status)
        {					
            call(Types.PUSH_ANALYTICS_SERVER, ANALYTICS_SETTING.merge({
                events : AnalyticsEvents.events
            }));	
        }

        AnalyticsEvents.events = [];

    }, timeout);
};

// PAGE VIEWS
const pageViewsAnalytics = (seo:any) => 
{	
    registerEvent({
        action : Types.PAGE_VIEWS_ACTION_ANALYTICS,
        keys   : [ seo?.title ],
    });	
}

const categoryClickAnalytics = (category:any) => 
{	
    if (category)
    {
        registerEvent({
            action   : Types.CATEGORY_CLICK_ACTION_ANALYTICS,
            keys     : [ category.name ],
            document : {
                referencePath : category.referencePath
            }
        });		
    }
}

const brandClickAnalytics = (brand:any) => 
{	
    if (brand)
    {
        registerEvent({
            action   : Types.BRAND_CLICK_ACTION_ANALYTICS,
            keys     : [ brand.name ],
            document : {
                referencePath : brand.referencePath
            }
        });		
    }
}

const productsViewsAnalytics = (documents:any) => 
{
    const items = [];
    const keys  = [];

    for (const item of documents)
    {
        items.push({
            referencePath : item.referencePath
        });
        keys.push(item.code + ": " + item.name);
    }

    registerEvent({
        action    : Types.PRODUCT_VIEWS_ACTION_ANALYTICS,
        keys      : keys,
        documents : items
    });
}

const productsVariantViewsAnalytics = (documents:any) => 
{
    const items = [];
    const keys  = [];

    for (const item of documents)
    {
        if (item._parent)
        {
            items.push({
                referencePath : item._parent.referencePath
            });	
        }

        keys.push(item.code + ": " + item.name);
    }

    registerEvent({
        action    : Types.PRODUCT_VIEWS_ACTION_ANALYTICS,
        keys      : keys,
        documents : items
    });
}

const userLoginAnalytics = (user:any) => 
{
    registerEvent({
        action : Types.USER_LOGIN_ACTION_ANALYTICS,
        keys   : [ user.email ],
    });	
}

const initCheckoutAnalytics = () => 
{
    registerEvent({
        action : Types.INIT_CHECKOUT_ACTION_ANALYTICS,
        keys   : [],
    });	
}

const couponCheckoutAnalytics = (coupon:string) => 
{
    registerEvent({
        action : Types.COUPON_CHECKOUT_ACTION_ANALYTICS,
        keys   : [ coupon ],
    });	
}

const addressCheckoutAnalytics = () => 
{
    registerEvent({
        action : Types.ADDRESS_CHECKOUT_ACTION_ANALYTICS,
        keys   : [],
    });	
}

const shippingCheckoutAnalytics = (name:string) => 
{
    registerEvent({
        action : Types.SHIPPING_CHECKOUT_ACTION_ANALYTICS,
        keys   : [ name ],
    });	
}

const paymentMethodCheckoutAnalytics = (name:string) => 
{
    registerEvent({
        action : Types.PAYMENT_METHOD_CHECKOUT_ACTION_ANALYTICS,
        keys   : [ name ],
    });	
}

const finishedCheckoutAnalytics = () => 
{
    registerEvent({
        action : Types.FINISHED_CHECKOUT_ACTION_ANALYTICS,
        keys   : [],
    }, 0);	
}

const addItemCartAnalytics = (product:any, variant:any) => 
{
    // PRODUTO
    registerEvent({
        action   : Types.ADD_PRODUCT_CART_ACTION_ANALYTICS,
        keys     : [ product.code + ": " + product.name ],
        document : {
            referencePath : product.referencePath
        }
    });	

    // // SKU
    // let sku = product.code;

    // if(variant && variant.length > 0)
    // {
    // 	for(const item of variant)
    // 	{
    // 		if(item && item.value)
    // 		{
    // 			sku += '-' + item.value;
    // 		}			
    // 	}		
    // }

    // registerEvent({
    // 	action : Types.ADD_SKU_CART_ACTION_ANALYTICS,
    // 	keys   : [sku],
    // 	document : {
    // 		referencePath : product.referencePath
    // 	}
    // });	

    // VARIANT
    if (variant && variant.length > 0)
    {
        let full = "";
        const subKeys = [];
        const keys    = [];

        for (const key in variant)
        {
            const item = variant[key];		
			
            if (item && item.label)
            {
                keys.push(item.label);
                subKeys.push("level-" + (parseInt(key) + 1));
                full += item.label + (parseInt(key) == variant.length - 1 ? "" : " - ");	
            }			
        }

        if (full)
        {
            keys.push(full);
            subKeys.push("full")
        }

        registerEvent({
            action   : Types.ADD_VARIANT_CART_ACTION_ANALYTICS,
            keys     : keys,
            subKeys  : subKeys,
            document : {
                referencePath : product.referencePath
            }
        });
    }
}

const productPurchasesAnalytics = (order:any) => 
{
    const documents = [];
    const keysProduct  = [];

    for (const item of order.items)
    {
        documents.push({
            referencePath : item.product?.referencePath
        });	

        keysProduct.push(item.code + ": " + item.name);

        // VARIANT
        if (item.variant && item.variant.length > 0)
        {
            let full = "";
            const keys = [];
            const subKeys = [];

            for (const key in item.variant)
            {
                const variant = item.variant[key];		
					
                if (variant && variant.label)
                {
                    keys.push(variant.label);
                    subKeys.push("level-" + (parseInt(key) + 1));

                    full += variant.label + (parseInt(key) == item.variant.length - 1 ? "" : " - ");	
                }
            }

            keys.push(full);
            subKeys.push("full");

            registerEvent({
                action   : Types.PURCHASES_VARIANT_ACTION_ANALYTICS,
                keys     : keys,
                subKeys  : subKeys,
                document : {
                    referencePath : item.product?.referencePath
                }
            });
        }
    }

    registerEvent({
        action    : Types.PRODUCT_PURCHASES_ACTION_ANALYTICS,
        keys      : keysProduct,
        documents : documents
    });
}

export { 
    registerEvent,
    productPurchasesAnalytics,
    pageViewsAnalytics,
    productsViewsAnalytics,
    userLoginAnalytics,
    addItemCartAnalytics,
    initCheckoutAnalytics,
    couponCheckoutAnalytics,
    addressCheckoutAnalytics,
    shippingCheckoutAnalytics,
    paymentMethodCheckoutAnalytics,
    finishedCheckoutAnalytics,
    categoryClickAnalytics,
    brandClickAnalytics,
    productsVariantViewsAnalytics,
}