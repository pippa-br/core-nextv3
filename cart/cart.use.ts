"use client"

import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";
import { useSearchParams } from "next/navigation";
import { useCore } from "../core/core";

const useGetCart = (setting:ISetting, onSuccess:any) =>  
{
    const { setIsLoadingCart } = useCore();

    useEffect(() => 
    {
        setIsLoadingCart(false);
		
        call(Types.GET_CART_SERVER, setting).then((result) => 
        {
            onSuccess(result.data);

            setIsLoadingCart(true);			
        });
		
    }, []);	
}

const useMergeCart = (setting:ISetting, onSuccess:any) =>  
{
    const searchParams = useSearchParams()
    const { cart } = useCore();
    const document = searchParams.get("document"); 

    useEffect(() => 
    {
        if (document && cart)
        {
            call(Types.MERGE_CART_SERVER, setting.merge(
                {
                    document : {
                        referencePath : document
                    }
                })).then((result) => 
            {
                onSuccess(result.data);
            });
        }

    }, [ document, cart ]);
}

const useSetStoreCart = (setting:ISetting, onSuccess?:any) => 
{
    useEffect(() => 
    {
        call(Types.SET_STORE_SERVER, setting).then((result) => 
        {
            if (onSuccess)
            {
                onSuccess(result.data)
            }			
        });	
    }, []);
}

const useSetCouponCart = (setting:ISetting, onSuccess?:any) => 
{
    useEffect(() => 
    {
        call(Types.SET_COUPON_CART, setting).then((result) => 
        {
            if (onSuccess)
            {
                onSuccess(result.data)
            }			
        });	
    }, []);
}

const useSetSellerCart = (setting:ISetting, onSuccess?:any) => 
{
    useEffect(() => 
    {
        call(Types.SET_SELLER_CART, setting).then((result) => 
        {
            if (onSuccess)
            {
                onSuccess(result.data)
            }			
        });	
    }, []);
}

export { 
    useGetCart,
    useSetStoreCart, 
    useMergeCart,	
    useSetCouponCart,
    useSetSellerCart,	
}