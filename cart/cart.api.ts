import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const getCart = async (setting: ISetting) => 
{
    const result = await call(Types.GET_CART_SERVER, setting);
    return result;
};

const mergeCart = async (setting: ISetting) => 
{
    const result = await call(Types.MERGE_CART_SERVER, setting);
    return result;
};

const mergeAllCart = async (setting: ISetting) => 
{
    const result = await call(Types.MERGE_ALL_CART_SERVER, setting);
    return result;
};

const validateCart = async (setting: ISetting) => 
{
    const result = await call(Types.VALIDATE_CART_SERVER, setting);
    return result;
};

const setInstallmentCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_INSTALLMENT_CART_SERVER, setting);
	
    return result;
}

const setInstallmentMethods = async (setting:ISetting) =>
{
    const result = await call(Types.SET_INSTALLMENT_METHODS_SERVER, setting);

    return result;
}

const checkoutCart = async (setting:ISetting) => 
{
    const result = await call(Types.CHECKOUT_CART_SERVER, setting);
	
    return result;
}

const setCouponCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_COUPON_CART, setting);
	
    return result;
}

const delCouponCart = async (setting:ISetting) => 
{
    const result = await call(Types.DEL_COUPON_CART, setting);
	
    return result;
}

const setDiscountClientCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_DISCOUNT_CLIENT_CART, setting);
	
    return result;
}

const delDiscountClientCart = async (setting:ISetting) => 
{
    const result = await call(Types.DEL_DISCOUNT_CLIENT_CART, setting);
	
    return result;
}

const setAddressCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_ADDRESS_CART_SERVER, setting);
	
    return result;
}

const setShippingMethodCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_SHIPPING_METHOD_CART_SERVER, setting);
	
    return result;
}

const setPaymentMethodCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_PAYMENT_METHOD_CART_SERVER, setting);
	
    return result;
}

const setCreditCardCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_CREDITCARD_CART_SERVER, setting);
	
    return result;
}

const setAttachmentCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_ATTACHMENT_CART_SERVER, setting);
	
    return result;
}

const clearCart = async (setting:ISetting) => 
{
    const result = await call(Types.CLEAR_CART_SERVER, setting);
	
    return result;
}

const setItemCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_ITEM_CART_SERVER, setting);
	
    return result;
}

const setPackaging = async (setting:ISetting) => 
{
    const result = await call(Types.SET_PACKAGING_CART_SERVER, setting);
	
    return result;
}

const setGiftCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_GIFT_CART_SERVER, setting);
	
    return result;
}

const setItemsCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_ITEMS_CART_SERVER, setting);
	
    return result;
}

const setClientCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_CLIENT_CART_SERVER, setting);
	
    return result;
}

const setReferrerCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_REFERRER_CART_SERVER, setting);
	
    return result;
}

const setUTMCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_UTM_CART_SERVER, setting);
	
    return result;
}

const setDiscountCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_DISCOUNT_CART_SERVER, setting);
	
    return result;
}

const setSellerCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_SELLER_CART_SERVER, setting);
	
    return result;
}

const delItemCart = async (setting:ISetting) => 
{
    const result = await call(Types.DEL_ITEM_CART_SERVER, setting);
	
    return result;
}

const delGiftCart = async (setting:ISetting) => 
{
    const result = await call(Types.DEL_GIFT_CART_SERVER, setting);
	
    return result;
}

const delItemsByStoreCart = async (setting:ISetting) => 
{
    const result = await call(Types.DEL_ITEMS_BY_STORE_CART_SERVER, setting);
	
    return result;
}

const delItemsByProductCart = async (setting:ISetting) => 
{
    const result = await call(Types.DEL_ITEMS_BY_PRODUCT_CART_SERVER, setting);
	
    return result;
}

const setAddressReferenceCart = async (setting:ISetting) => 
{
    const result = await call(Types.SET_ADDRESS_REFERENCE_CART_SERVER, setting);
	
    return result;
}

const setCreditCardReferenceCart = async (setting: ISetting) => 
{
    const result = await call(Types.SET_CREDITCARD_REFERENCE_CART_SERVER, setting);
  
    return result;
};

const calculateZipCodeCart = async (setting: ISetting) => 
{
    const result = await call(Types.CALCULATE_ZIP_CODE_SERVER, setting);
  
    return result;
};

const setItemReorderCart = async (setting: ISetting) => 
{
    const result = await call(Types.SET_ITEM_REORDER_CART_SERVER, setting)
    return result
}

const delItemReorderCart = async (setting: ISetting) => 
{
    const result = await call(Types.DEL_ITEM_REORDER_CART_SERVER, setting)
    return result
}

const setItemsReorderCart = async (setting: ISetting) =>
{
    const result = await call(Types.SET_ITEMS_REORDER_CART_SERVER, setting)
    return result
}

const setDiscountReoderCart = async (setting: ISetting) =>
{
    const result = await call(Types.SET_DISCOUNT_REORDER_CART_SERVER, setting)
    return result
}

const setAgentCart = async (setting: ISetting) => 
{
    const result = await call(Types.SET_AGENT_CART_SERVER, setting)
    return result
}

const setNoteCart = async (setting: ISetting) =>
{
    const result = await call(Types.SET_NOTE_CART_SERVER, setting)
    return result
}

export { 
    getCart,
    mergeCart,
    setAddressReferenceCart,	
    setCreditCardReferenceCart, 
    delItemsByProductCart,
    setItemsCart, 
    delItemCart, 
    checkoutCart, 
    clearCart, 
    setAddressCart, 
    setInstallmentCart,
    setInstallmentMethods, 
    setItemCart,
    setShippingMethodCart, 
    setAttachmentCart, 
    setPaymentMethodCart, 
    setCreditCardCart, 
    setCouponCart, 
    setDiscountClientCart,
    delDiscountClientCart,
    validateCart,
    calculateZipCodeCart,
    delItemsByStoreCart,
    setSellerCart,
    setClientCart,
    setDiscountCart,
    delCouponCart,
    mergeAllCart,
    setGiftCart,
    delGiftCart,
    setReferrerCart,
    setPackaging,
    setUTMCart,
    setItemReorderCart,
    delItemReorderCart,
    setItemsReorderCart,
    setDiscountReoderCart,
    setAgentCart,
    setNoteCart,
}