import Types from "../type";
import { callFormData } from "../util/call.api";

declare const FormData:any;

const uploadStorage = async (setting:any) => 
{
    const form = new FormData();
    const keys = Object.keys(setting);

    for (let i = 0; i < keys.length; i++)
    {
        form.append(keys[i], setting[keys[i]]);
    }

    const result = await callFormData(Types.UPLOAD_STORAGE_SERVER, form);
	
    return result;	
}

export { uploadStorage }