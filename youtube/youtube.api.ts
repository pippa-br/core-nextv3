import Types from "../type";
import { call } from "../util/call.api";
import { useEffect } from "react";
 
const loadVideosByChannelYoutube = (setting:any) => 
{
    let baseUrl = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + setting.channelId + "&key=" + setting.apiKey + "&q=" + (setting.term ? setting.term : "") + "&maxResults=" + (setting.perPage ? setting.perPage : 48) + "&order=date";

    if (setting.pageToken)
    {
        baseUrl += "&pageToken=" + setting.pageToken;
    }

    //baseUrl = 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);

    const data = {
        url : baseUrl,
    };

    return call(Types.PROXY_UTIL_SERVER, data)
} 

const useGetVideosByChannelYoutube = (setting:any, onSuccess:any) => 
{
    useEffect(() => 
    {
        loadVideosByChannelYoutube(setting).then(result => 
        {
            onSuccess(result);
        })
    }, []);	
}

const loadVideosByPlaylistYoutube = (setting:any) => 
{
    let baseUrl = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=" + setting.playlistId + "&key=" + setting.apiKey + "&q=" + (setting.term ? setting.term : "") + "&maxResults=" + (setting.perPage ? setting.perPage : 48) + "&order=date";

    if (setting.pageToken)
    {
        baseUrl += "&pageToken=" + setting.pageToken;
    }

    //baseUrl = 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);

    const data = {
        url : baseUrl,
    };

    return call(Types.PROXY_UTIL_SERVER, data)
}

const useGetVideosByPlaylistYoutube = (setting:any, onSuccess:any) => 
{
    useEffect(() => 
    {
        loadVideosByPlaylistYoutube(setting).then(result => 
        {
            onSuccess(result);
        })
    }, []);	
}

export { 
    useGetVideosByChannelYoutube,
    useGetVideosByPlaylistYoutube,
    loadVideosByChannelYoutube,
    loadVideosByPlaylistYoutube
}