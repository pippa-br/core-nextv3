import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

// const addWalletByReorderTotal = async (setting:ISetting) => {
//     const result = await call(Types.ADD_WALLET_BY_REORDER_TOTAL_SERVER, setting);
//     return result;
// }

const addWalletByDocument = async (setting:ISetting) => 
{
    const result = await call(Types.ADD_WALLET_BY_DOCUMENT, setting);
    return result;
}

const addWallet = async (setting:ISetting) => 
{
    const result = await call(Types.ADD_WALLET, setting);
    return result;
}

export {
    //addPointsByReorderTotal,
    addWalletByDocument,
    addWallet
}
