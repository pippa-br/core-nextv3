import Types from "../type";
import { call } from "../util/call.api";
import { ISetting } from "../util/setting";

const getBarChart = async (setting:ISetting) => 
{
    const result = await call(Types.BAR_CHART_SERVER, setting);
    return result;
}

const getCard = async (setting: ISetting) =>
{
    const result = await call(Types.CARD_SERVER, setting)
    return result
}

export { getBarChart, getCard }
