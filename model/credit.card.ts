import BaseModel from "./base.model";

export class CreditCard extends BaseModel
{
    populate(data:any)
    {
        this.expirydate    = data.creditCard.expirydate;
        this.referencePath = data.referencePath;
        this.owner		   = data.creditCard.owner;
        this.cardnumber    =  data.creditCard.last4 ? data.creditCard.last4 : `**** **** **** ${data.creditCard.cardnumber.slice(data.creditCard.cardnumber.length - 4)}`;
    }
}