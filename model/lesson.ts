import BaseModel 			from "./base.model";
import User      			from "./user";
import { LessonCollection } from "./lesson.collection";
import Form 				from "./form";

export class Lesson extends BaseModel
{	
    getMaps()
    {
        return {
            form      : { klass : Form },
            owner     : { klass : User },
            model     : { klass : Lesson },
            _children : { klass : LessonCollection },
            prev   	  : { klass : Lesson },
            next   	  : { klass : Lesson }
        };
    }

    set _children(value:any)
    {
        this.__children = new LessonCollection(value);
    }

    get _children()
    {
        return this.__children;
    }	

    initialize()
    {
        //this.minPercentage = ( this.core().account.minPercentage != undefined ? this.core().account.minPercentage : 80);
        this.minPercentage = 80;
        this["percentage"] = -1;
        this["isFirst"]    = false;
        this["isLast"]     = false;
        this["hasSum"]     = false;
    }

    get actived()
    {
        return this["percentage"] >= this.minPercentage || !this["hasSum"];
    }

    get current()
    {
        return (this["percentage"] < this.minPercentage && this.isFirst) || (this["percentage"] < this.minPercentage && this.prev && this.prev.actived);
    }
}
