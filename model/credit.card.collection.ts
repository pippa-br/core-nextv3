import { BaseCollection } from "./base.collection";
import { CreditCard } 	  from "./credit.card";

export class CreditCardCollection extends BaseCollection
{
    public getMapsModel()
    {
        return {
            model  	   : CreditCard,
            collection : CreditCardCollection
        };
    }
}