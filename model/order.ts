import { TDate } from "./TDate";
import BaseModel from "./base.model";

export default class Order extends BaseModel
{
    validateReorder()
    {
        let days = 0

        if (this.reorderExpiryDate)
        {
            const dateStart = new TDate();
            const dateEnd   = new TDate({  value : this.reorderExpiryDate });
	
            days = dateEnd.diff(dateStart, "days");
        }

        return this.statusPayment == "Pago" && this.hasEnotas && !this.hasReorder && days >= 0;			
    }
}