import { BaseCollection } from "./base.collection";
import Order from "./order";

export class OrderCollection extends BaseCollection
{
    getMapsModel()
    {
        return {
            model    		: Order,
            collection : OrderCollection,
        }
    }
}