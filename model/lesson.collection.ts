/* APP */
import { BaseCollection } from "./base.collection";
import { Lesson } from "./lesson";

export class LessonCollection extends BaseCollection
{
    getMapsModel() : any
    {
        return {
            collection : LessonCollection,
            model  	   : Lesson,			
        };
    }

    getFlat()
    {
        const items = [];

        for (const item of this)
        {
            if (item._children && item._children.length > 0)
            {
                for (const item2 of item._children)
                {
                    item2.parent = item;
					
                    if (item2.hasSum)
                    {						
                        items.push(item2);	
                    }
                }
            }
            else
            {
                if (item.hasSum)
                {
                    items.push(item);							
                }				
            }			
        }

        return items;
    }

    getPercentage()
    {
        const items = this.getFlat();
        let count = 0;

        for (const key in items)
        {
            if (items[key].actived)
            {
                count++;
            }
        }

        return (count * 100 / items.length).toFixed(2);
    }

    // FAZ NO FROTN PARA MANTER PONTEIROS DE PREV E NEXT
    normalize()
    {
        const items = this.getFlat();

        /* SET PREV NEXT */
        for (const key in items)
        {
            const i = parseInt(key);

            if (i == 0)
            {
                items[i].isFirst = true;
            }

            if (i == items.length - 1)
            {
                items[i].isLast = true;
            }

            if (items[i].isFirst && items[i].isLast)
            {
                // NÃO TEM PREV NEM NEXT
            }
            else if (items[i].isFirst)
            {
                items[i].next = items[i + 1];
            }
            else if (items[i].isLast)
            {
                items[i].prev = items[i - 1];
            }
            else
            {
                items[i].next = items[i + 1];
                items[i].prev = items[i - 1];
            }
        }
    }
	
    getCurrent()
    {
        const items = this.getFlat();

        for (const key in items)
        {
            if (items[key].current)
            {
                return items[key];
            }
        }

        // RETORNA O ULTIMO
        if (items.length > 0)
        {
            return items[items.length - 1];			
        }
    }

    /*setNextCurrent()
	{
		for(const key in this)
		{
			const item : any = this[key];

			if(item._children && item._children.length > 0)
            {
                for(const key2 in item._children)
                {
					const item2 = item._children[key2]

                    if(!item2.actived && !item2.current)
					{	
						item2.current = true;
						return item2;
					}

					/* DESABILITA O ATUAL */
    /*if(item2.current)
					{
						item2.current = false;
					}
                }
            }
            else
            {
                if(!item.actived)
				{				
					item.current = true;
					return item;
				}
			}					
		}
	}*/
}
