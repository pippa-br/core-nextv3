class UseLanguage
{
    static words:any = {};
    static language:any = "pt";
}

const parseCollection = (collection:any) => 
{

    const newCollection:any = {};
    
    let auxObj:any = {}
    
    for (let index = 0; index < collection?.length; index++)
    {
        const obj = Object.keys(collection[index]);
        const key = collection[index]["pt"];

        for (let indexKey = 0; indexKey < obj.length; indexKey++)
        {
            if (obj[indexKey] !== "referencePath")
            {
                auxObj[`${obj[indexKey]}`] = collection[index][`${obj[indexKey]}`]; 
            }
        }

        newCollection[`${key}`] = auxObj;
        auxObj = {};
    }

    return newCollection;
}

export const setSelectedLanguage = (option:string, collection:any) => 
{
    if (option)
    {
        UseLanguage.language = option;
        UseLanguage.words = parseCollection(collection);
        return true;
    }
    else 
    {
        console.error("Invalid Option type, received value :", String(option));
        return false;
    }
}

export const translate = (word:string) => 
{

    const wordList = UseLanguage.words;

    const arr = Object.keys(wordList);

    if (arr?.length > 0)
    {
        for (let index = 0; index < arr.length; index++)
        {

            if (arr[index] === wordList[`${word}`]?.pt)
            {
                const translatedWord = wordList[word][UseLanguage.language];
                return String(translatedWord);
            }
        }
    
        if (word !== undefined) console.log("Word Not registered", word);
        
        return word;
    }
    else 
    {
        return word;
    }
}
