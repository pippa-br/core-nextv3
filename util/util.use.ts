"use client"

//import axios from "axios";
import { useCookies } from "react-cookie"
import { useEffect } from "react"
import { usePathname } from "next/navigation";
import { useRouter } from "next/navigation";

declare const window: any;
declare const document: any;
declare const performance:any;

// UNIQUE USER
const useSetUTMParameters = (onSuccess?: any) => 
{
    const [ cookies, setCookie ] = useCookies([
        "utm_source",
        "utm_medium",
        "utm_campaign",
        "utm_term",
        "utm_content",
        "referrer",
    ])

    cookies;
    
    const currentTimeMs = performance.now();
    const expiresTimeMs = currentTimeMs + 24 * 60 * 60 * 1000; // + 24 horas
    const expires			  = new Date(expiresTimeMs);

    useEffect(() => 
    {
        // Obtém a string da URL
        const queryString = window.location.search

        // Cria um objeto URLSearchParams para analisar os parâmetros
        const params = new URLSearchParams(queryString)

        // Obtém os valores dos parâmetros UTM
        const utmSource = params.get("utm_source")
        const utmMedium = params.get("utm_medium")
        const utmCampaign = params.get("utm_campaign")
        const utmTerm = params.get("utm_term")
        const utmContent = params.get("utm_content")
        const referrer = document.referrer || null

        // Faça algo com os valores UTM
        console.log("UTM Source:", utmSource)
        console.log("UTM Medium:", utmMedium)
        console.log("UTM Campaign:", utmCampaign)
        console.log("UTM Term:", utmTerm)
        console.log("UTM Content:", utmContent)

        const data: any = {
            utmSource   : utmSource,
            utmMedium   : utmMedium,
            utmCampaign : utmCampaign,
            utmTerm     : utmTerm,
            utmContent  : utmContent,
            referrer    : referrer,
        }

        if (utmSource) 
        {
            setCookie("utm_source", utmSource, { path : "/", expires })
        }

        if (utmMedium) 
        {
            setCookie("utm_medium", utmMedium, { path : "/", expires })
        }

        if (utmCampaign) 
        {
            setCookie("utm_campaign", utmCampaign, { path : "/", expires })
        }

        if (utmTerm) 
        {
            setCookie("utm_campaign", utmTerm, { path : "/", expires })
        }

        if (utmContent) 
        {
            setCookie("utm_content", utmContent, { path : "/", expires })
        }

        if (referrer && referrer.indexOf(window.location.host) > -1) 
        {
            setCookie("referrer", referrer, { path : "/", expires })
        }

        if (onSuccess) 
        {
            onSuccess(data)
        }
    }, [])
}

const useGetUTMParameters = (onSuccess: any) => 
{
    const [ cookies, setCookie ] = useCookies([
        "utm_source",
        "utm_medium",
        "utm_campaign",
        "utm_term",
        "utm_content",
        "referrer",
    ])

    setCookie;

    useEffect(() => 
    {
        const data: any = {}

        if (cookies.utm_source) 
        {
            data.utm_source = cookies.utm_source
        }

        if (cookies.utm_medium) 
        {
            data.utm_medium = cookies.utm_medium
        }

        if (cookies.utm_campaign) 
        {
            data.utm_campaign = cookies.utm_campaign
        }

        if (cookies.utm_term) 
        {
            data.utm_term = cookies.utm_term
        }

        if (cookies.utm_content) 
        {
            data.utm_content = cookies.utm_content
        }

        if (cookies.referrer) 
        {
            data.referrer = cookies.referrer
        }

        onSuccess(data)
    }, [])
}

const useCustomPush = () =>
{
    const router = useRouter();
    const currentPathname = usePathname();

    const customPush = (pathname: string) => 
    {
        const isAppMobile = currentPathname.startsWith("/mobile");
        router.push((isAppMobile ? "/mobile" : "") + pathname);
    };

    return customPush;
}

export {	
    useSetUTMParameters,
    useGetUTMParameters,	
    useCustomPush,
}
