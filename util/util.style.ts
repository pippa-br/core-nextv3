"use client"

import { useCallback, useEffect, useState } from "react";
declare const window: any;

const customSelectStyles: any = {
    control : (base: any, state: any) => ({
        ...base,
        borderRadius : "var(--border-radius)",
        borderColor  : state.isFocused
            ? "var(--border-color)"
            : "var(--border-color)",
        boxShadow : state.isFocused ? "none" : null,
        "&:hover" : {
            borderColor : "var(--border-color)",
        },      
        color           : "var(--select-color, #000)",
        backgroundColor : "var(--select-background-color, #fff)",
        padding         : "0 1rem",
        fontSize        : "16px"  // Evita zoom ao focar no input
    }),
    container : (base: any) => ({
        ...base,  
        width : "100%",
    }),
    singleValue : (styles: any) => ({ 
        ...styles, 
        color     : "var(--select-color, #000)", 
        textAlign : "left",
    }),
    indicatorsContainer : (styles: any) => ({
        ...styles,
        color   : "var(--select-color, #000)",
        padding : "0",  
    }),
    indicatorSeparator : () => ({
        display : "none"
    }),    
    placeholder : (styles: any) => ({
        ...styles,
        color     : "var(--select-placeholder, #808080)",
        textAlign : "left",        
    }),
    clearIndicator : (styles: any) => ({
        ...styles,
        padding    : "0 5px 0 0",
        height     : "100%",
        alignItems : "center",
        display    : "flex",
    }),
    dropdownIndicator : (styles: any) => ({
        ...styles,
        padding    : "0 5px 0 0",
        height     : "100%",
        alignItems : "center",
        display    : "flex",
    }),
    valueContainer : (styles: any) => ({
        ...styles,
        padding   : "0 0 0 5px",
        textAlign : "center"
    }),     
    menu : (styles: any) => ({
        ...styles,
        margin : "0",
        zIndex : 999,
    }),     
    input : (provided: any) => ({
        ...provided,
        fontSize : "16px", // Evita zoom ao focar no input
    }),
    option : (provided: any) => ({
        ...provided,
        fontSize : "16px", // Também ajusta o tamanho das opções
    }),
};

const useHeaderState = () => 
{
    const [ show, setShow ] = useState(false);
  
    const getViewPosition = useCallback(() => 
    {
        if (window.scrollY >= 300) 
        {
            setShow(true);
        }
        else 
        {
            setShow(false);
        }
  
    }, [ show ]);
  
    useEffect(() => 
    {
        window.addEventListener("scroll", getViewPosition, false);
  
        return () => 
        {
            window.removeEventListener("scroll", getViewPosition, false);
        };
  
    }, []);
  
    return show;
};

const useShowFooter = () => 
{
    const [ show, setShow ] = useState(true);
    const [ lastScrollY, setLastScrollY ] = useState(0);
  
    const controlNavbar = () => 
    {
        if (typeof window !== "undefined") 
        {
            if (window.scrollY > lastScrollY) 
            {
                if (window.scrollY - lastScrollY > 20)
                {
                    setShow(false);
                }        
            }
            else 
            {
                setShow(true);
            }
  
            setLastScrollY(window.scrollY);
        }
    };
  
    useEffect(() => 
    {
        if (typeof window !== "undefined") 
        {
            window.addEventListener("scroll", controlNavbar);
  
            return () => 
            {
                window.removeEventListener("scroll", controlNavbar);
            };
        }
    }, [ lastScrollY ]);
  
    return show;
};

export {
    customSelectStyles,
    useHeaderState,
    useShowFooter
}