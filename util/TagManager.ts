import TagManager from "react-gtm-module";
import sha256 from "js-sha256";
import slugify from "slugify";
import { productPrice, productPromotionalPrice } from "../price/price.util";

export const tagManager =
{
    initialize : (gtm: any) => 
    {
		
        const tagManagerArgs =
		{
		    gtmId : gtm.key,
		};

        try 
        {
            TagManager.initialize(tagManagerArgs);
        }
        catch (e:any)
        {
            console.error("Error TagManager", e);
        }
    },

    pageView : (
        path: string, // /produto/abs-123
        title: string, // Produto Legal
        area: string, // b2b
        segment: string, // ecommerce
        // category, // roupas
        // subCategory, // vestidos
        environment: string, // app | desktop | mobile
        platform: string, // binext 1.0
        // type = '', // home|erro|atendimento
        user: any) => // usuário logado
    {
        const data: any =
		{
		    event : "pageView",
		    page :
			{
			    pagePath    : path,
			    name        : title,
			    area        : area,
			    segment     : segment,
			    // category: category,
			    // subCategory: subCategory,
			    environment : environment,
			    platform    : platform,
			    // type: type
			},

		};

        if (user) 
        {
            data.user = tagManager.parseUser(user);
        }

        TagManager.dataLayer(
            {
                dataLayer : data
            });
    },
    registerEvent : (event: string, component: string, name: string, target = 0, user: any) => 
    {
        const data: any =
		{
		    event : "interaction",
		    interaction :
			{
			    component   : slugify(component, { lower : true }), //menu, cart, etc
			    action      : slugify(event, { lower : true }), //click, modal, etc
			    description : slugify(name || "", { lower : true }),
			    value       : (target + "").toLowerCase()
			}
		};

        if (user) 
        {
            data.user = tagManager.parseUser(user);
        }

        TagManager.dataLayer(
            {
                dataLayer : data
            });
    },
    registerNonInteractiveEvent : (event: string, component: string, name: string, target = 0, user: any) => 
    {
        const data: any =
		{
		    event : "noninteraction",
		    interaction :
			{
			    component   : slugify(component, { lower : true }), //menu, cart, etc
			    action      : slugify(event, { lower : true }), //click, modal, etc
			    description : slugify(name, { lower : true }),
			    value       : (target + "").toLowerCase()
			}
		};

        if (user) 
        {
            data.user = tagManager.parseUser(user);
        }

        TagManager.dataLayer(
            {
                dataLayer : data
            });
    },
    viewBanner : (banner: any, position = 0) => 
    {
        if (!banner.image) 
        {
            banner.image = banner.desktop;
        }

        let filename = banner.image.name.split(".");
        filename.pop();
        filename = filename.join(".");
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event : "promotionView",
				    ecommerce :
					{
					    promoView :
						{
						    promotions :
								[
								    {
								        id       : banner.id,
								        name     : slugify(banner.name ? banner.name : filename, { lower : true }),
								        creative : filename,
								        position : position
								    }
								]
						}
					}
				}
            });
    },
    clickBanner : (banner: any, user = null) => 
    {
        if (!banner.image) 
        {
            banner.image = banner.desktop;
        }

        let filename = banner.image.name.split(".");
        filename.pop();
        filename = filename.join(".");
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event : "promotionClick",
				    ecommerce :
					{
					    promoClick :
						{
						    promotions :
								[
								    {
								        id       : banner.id,
								        name     : slugify(banner.name ? banner.name : filename, { lower : true }),
								        creative : filename,
								        position : banner.order
								    }
								]
						}
					}
				}
            });
        tagManager.registerEvent("click-link", "banner", banner.name ? banner.name : filename, 0, user);
    },
    productList : (products: Array<any>) => 
    {
        const productList: Array<any> = [];
        const fbProductList: Array<any> = [];
        products.forEach((product) => 
        {
            productList.push(tagManager.newParse(product));
            fbProductList.push(tagManager.facebookParse(product));
        });
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event : "productImpression",
				    ecommerce :
					{
					    currencyCode : "BRL",
					    impressions  : productList
					},
				    pageTitle : "Produtos",
				    facebook_ecommerce :
					{
					    items : fbProductList,
					},
				},
            });
    },
    productClick : (product: any, selector: string) => 
    {
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event : "productClick",
				    ecommerce :
					{
					    click :
						{
						    actionField : { list : [ selector ] },
						    products    : [ tagManager.newParse(product) ]
						}
					},
				    facebook_ecommerce :
					{
					    items : [ tagManager.facebookParse(product) ],
					},
				},
            });
    },
    productView : (product: any, selector: string, user: any = null) => 
    {
        let unavailable = false;

        if (product.stockTable) 
        {
            unavailable = true;
            const keys = Object.keys(product.stockTable.data);

            for (const key of keys) 
            {
                if (product.stockTable.data[key].quantity > 0) 
                {
                    unavailable = false;
                    break;
                }
            }
        }

        if (unavailable) 
        {
            tagManager.registerNonInteractiveEvent("view-product", "page", "produto-indisponivel:" + product.code + ":" + product.name, product.price, user)
        }
        else 
        {
            TagManager.dataLayer(
                {
                    dataLayer :
					{
					    event : "productDetail",
					    ecommerce :
						{
						    detail :
							{
							    actionField : { list : [ selector ] },
							    products    : [ tagManager.newParse(product) ]
							}
						},
					    // facebook_ecommerce:
					    // {
					    // 	items: [tagManager.facebookParse(product)],
					    // },
					},
                });
        }
    },
    customList : (list: Array<any>, event: string) => 
    {
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event : event,
				    ecommerce :
					{
					    currencyCode : "BRL",
					    impressions  : list
					}
				},
            });
    },
    customClick : (item: any, selector: string, event: string) => 
    {
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event : event,
				    ecommerce :
					{
					    click :
						{
						    actionField : { list : [ selector ] },
						    items       : [ item ]
						}
					},
				},
            });
    },
    customView : (item: any, category: string, event: string) => 
    {
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event : event,
				    ecommerce :
					{
					    detail :
						{
						    actionField : { list : [ slugify(category, { lower : true }) ] },
						    items       : [ item ]
						}
					}
				},
            });
    },
    cartView : (
        products: any,
        path: string, // /produto/abs-123
        title: string, // Produto Legal
        area: string, // b2b
        segment: string, // ecommerce
        environment: string, // app | desktop | mobile
        user: any) => // usuário logado) => 
    {
        const productList = [];
        const fbProductList = [];
        const productids = [];

        for (const product of products)
        {
            productList.push(tagManager.newParse(product));
            fbProductList.push(tagManager.facebookParse(product));
            productids.push(product.id);
        }

        const data: any =
		{
		    event : "CartView",
		    page :
			{
			    pagePath    : path,
			    name        : title,
			    area        : area,
			    segment     : segment, 
			    // category: 'marca', 
			    // subCategory: 'carrinho-usuario', 
			    environment : environment
			},
		    products : productList,
		    facebook_ecommerce :
			{
			    items : fbProductList
			},
		};

        if (user)
        {
            data.user = tagManager.parseUser(user);
        }

        TagManager.dataLayer(
            {
                dataLayer : data				
            });
    },
	
    checkoutStep : (products: any, event: string, step: number) => 
    {
        const productList = [];
        const fbProductList = [];
        const productids = [];

        for (const product of products) 
        {
            productList.push(tagManager.newParse(product));
            fbProductList.push(tagManager.facebookParse(product));
            productids.push(product.id);
        }

        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event : event,
				    ecommerce :
					{
					    checkout :
						{
						    actionField :
							{
							    step    : step,
							    cartIds : productids
							},
						    products : productList,
						}
					},
				    facebook_ecommerce :
					{
					    items : fbProductList
					},
				},
            });
    },
    addToCart : (pagePath: string, product: any, quantity: any, user: any) => 
    {
        const price: any = productPrice(product);
        tagManager.registerEvent("click-button", "box", "Adicionar ao carrinho:" + product.code + ":" + product.name, 0, user);
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event     : "addToCart",
				    pagePath  : pagePath,
				    pageTitle : "Produto",
				    ecommerce :
					{
					    currencyCode : "BRL",
					    add :
						{
						    products : tagManager.newParse(product, quantity),
						}
					},
				    facebook_ecommerce :
					{
					    items    : [ tagManager.facebookParse(product, quantity) ],
					    name     : product.name,
					    currency : "BRL",
					    value    : price * quantity
					},
				},
            });
    },
    removeFromCart : (pagePath: string, product: any, quantity: any) => 
    {
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event     : "removeFromCart",
				    pagePath  : pagePath,
				    pageTitle : "Produto",
				    ecommerce :
					{
					    currencyCode : "BRL",
					    remove :
						{
						    products : tagManager.parse(product, quantity),
						}
					},
				},
            });
    },
    addProduct : (pagePath: string, product: any) => 
    {
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event     : "view_item",
				    pagePath  : pagePath,
				    pageTitle : "Produto",
				    ecommerce :
					{
					    items : tagManager.parse(product),
					},
				    facebook_ecommerce :
					{
					    items : [ tagManager.facebookParse(product) ],
					},
				},
            });
    },
    addProducts : (pagePath: string, products: any) => 
    {
        if (products && products.length > 0) 
        {
            const gTagproducts = products.map((product: any) => 
            {
                return tagManager.parse(product);
            });
            const facebookProducts = products.map((product: any) => 
            {
                return tagManager.facebookParse(product);
            });
            TagManager.dataLayer(
                {
                    dataLayer :
					{
					    event     : "view_item_list",
					    pagePath  : pagePath,
					    pageTitle : "Produtos",
					    ecommerce :
						{
						    items : gTagproducts,
						},
					    facebook_ecommerce :
						{
						    items : facebookProducts,
						},
					},
                });
        }
    },
    checkout : (pagePath: string, products: any) => 
    {
        if (products) 
        {
            const gtagProducts = products.map((product: any) => 
            {
                return tagManager.parse(product);
            });
            let subtotal = 0;
            let productAmount = 0;
            const facebookProducts = products.map((product: any) => 
            {
                productAmount += product.quantity;
                subtotal += product.promotionalPrice
                    ? product.promotionalPrice * product.quantity
                    : product.price * product.quantity;
                return tagManager.facebookParse(product);
            });
            TagManager.dataLayer(
                {
                    dataLayer :
					{
					    event     : "begin_checkout",
					    pagePath  : pagePath,
					    pageTitle : "Checkout",
					    ecommerce :
						{
						    items : gtagProducts,
						},
					    facebook_ecommerce :
						{
						    items    : facebookProducts,
						    currency : "BRL",
						    quantity : productAmount,
						    value    : subtotal,
						},
					},
                });
        }
    },
    purchase : (order: any) => 
    {
        const gtagProducts = order.items.map((product: any) => 
        {
            return tagManager.parse(product);
        });
        const facebookProducts = order.items.map((product: any) => 
        {
            return tagManager.facebookParse(product);
        });
        TagManager.dataLayer(
            {
                dataLayer :
				{
				    event : "purchase",
				    ecommerce :
					{
					    purchase :
						{
						    actionField :
							{
							    id            : order.id,
							    affiliation   : "Nome da Loja",
							    revenue       : order.totalItems,
							    tax           : 0,
							    shipping      : order.totalShipping,
							    paymentMethod : order.paymentMethod.value,
							    screenID      : order._sequence
							},
						    products : gtagProducts
						},
					    facebook_ecommerce :
						{
						    items    : facebookProducts,
						    currency : "BRL",
						    value    : order.total,
						},
					},
				}
            });
    },
    newParse : (product: any, quantity = 1) => 
    {
        const item =
		{
		    name          : product.name ? product.name : product.product.name,
		    id            : product.id,
		    originalPrice : productPrice(product, null, false),
		    price         : productPromotionalPrice(product, null, false),
		    brand         : product.store ? product.store.name : "",
		    category      : product.categories && product.categories.length > 0 ? product.categories[0].name : "",
		    list          : "Produtos",
		    position      : product.order ? product.order : 1,
		    quantity      : quantity ? quantity : product.quantity,
		};
        return item;
    },
    parse : (product: any, quantity = null) => 
    {
        const item =
		{
		    name           : product.name ? product.name : product.product.name,
		    id             : product.id,
		    price          : product.promotionalPrice ? product.promotionalPrice : product.price,
		    item_category  : product.categories && product.categories.length > 0 ? product.categories[0].name : "",
		    item_list_name : "Produtos",
		    quantity       : quantity ? quantity : product.quantity,
		};
        return item;
    },
    facebookParse : (product: any, quantity = null) => 
    {
        const item =
		{
		    id       : product.id,
		    quantity : quantity ? quantity : product.quantity,
		};
        return item;
    },
    parseUser : (user: any) => 
    {
        const data: any =
		{
		    id         : user.id,
		    name       : sha256.sha256(user.name + ",s)S.X-p;SdwsA2&lR.dIy|SCg}bZ1{&7y^%kpk2u9V+{mEO%n3HccBYJIKhFujb"),
		    email      : sha256.sha256(user.email + ",s)S.X-p;SdwsA2&lR.dIy|SCg}bZ1{&7y^%kpk2u9V+{mEO%n3HccBYJIKhFujb"),
		    msisdn     : sha256.sha256(user.phone + ",s)S.X-p;SdwsA2&lR.dIy|SCg}bZ1{&7y^%kpk2u9V+{mEO%n3HccBYJIKhFujb"),
		    isLoggedIn : true
		}

        if (user.address) 
        {
            data.address =
			{
			    city       : user.address.city,
			    state      : user.address.state,
			    postalCode : user.address.zipcode,
			    country    : "BRA"
			}
        }

        if (user.cpf && user.cpf != "") 
        {
            data.cpf = sha256.sha256(user.cpf + ",s)S.X-p;SdwsA2&lR.dIy|SCg}bZ1{&7y^%kpk2u9V+{mEO%n3HccBYJIKhFujb");
        }
        else if (user.cnpj) 
        {
            data.cnpj = sha256.sha256(user.cnpj + ",s)S.X-p;SdwsA2&lR.dIy|SCg}bZ1{&7y^%kpk2u9V+{mEO%n3HccBYJIKhFujb");
        }

        return data;
    }
};
