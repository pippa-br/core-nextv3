import { create } from "zustand";

type FilterFieldsType = {
  [key: string]: any;
};

type FilterType = {
  filter: FilterFieldsType;
  callback:any;
  registerCallback:any;
  setFilter: any;
  resetFilters: () => void;
  reloadFilter: any;
  waitForReady: () => Promise<FilterFieldsType>;
  resetField: any;
  markReady: () => void;
};

const createFilter = (initialState: FilterFieldsType) =>
    create<FilterType>((set, get) => ({
        filter : {
            ...initialState,
            isReady : true,
        },
        callback         : null,
        registerCallback : (callback:any) => 
        {
            set(() => ({
                callback : callback,
            }));
        },
        setFilter : (key: string, value: any) => 
        {
            set((state) => ({
                filter : {
                    ...state.filter,
                    [key]   : value,
                    isReady : false,
                },
            }));
        },
        resetFilters : () => 
        {
            set(() => ({
                filter : {
                    ...initialState,
                    isReady : true,
                },
            }));
        },
        resetField : (key: string) => 
        {
            if (key in initialState) 
            {
                set((state) => ({
                    filter : {
                        ...state.filter,
                        [key] : initialState[key],
                    },
                }));
            }
            else 
            {
                console.warn(`Campo "${key}" não encontrado no estado inicial.`);
            }
        },
        markReady : () => 
        {
            set((state) => ({
                filter : { ...state.filter, isReady : true },
            }));
        },
        waitForReady : () => 
        {
            return new Promise((resolve) => 
            {
                const checkReady = () => 
                {
                    const { filter } = get();

                    if (filter.isReady) 
                    {
                        resolve(filter);
                    }
                    else 
                    {
                        setTimeout(checkReady, 10);
                    }
                };

                checkReady();
            });
        },
        reloadFilter : async () => 
        {
            return new Promise((resolve) => 
            {
                setTimeout(async () => 
                {
                    const { waitForReady, markReady, callback } = get();

                    if (callback)
                    {                        
                        markReady();

                        const filter = await waitForReady();

                        await callback(filter);
                    }                    

                    resolve(true)
                }, )                
            })            
        },
    }));

export default createFilter;