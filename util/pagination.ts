import { useEffect, useState } from "react";

class MapPages 
{
    static pages : any = {};
}

const Pagination = (fc:any) => 
{
    const [ currentPage, setCurrentPage ] = useState(1);
  	const [ totalPage,   setTotalPage ] 	= useState(0);
    const [ total,       setTotal ] 	    = useState(0);
    const [ hasNextPage, setHasNextPage ] = useState(false);
    const [ hasPrevPage, setHasPrevPage ] = useState(false);

    useEffect(() => 
    {
        // RODA NO INICIO
        if (totalPage)
        {
            fc(MapPages.pages[currentPage]);
            setHasNextPage(currentPage < totalPage);
            setHasPrevPage(currentPage > 1);
        }
    }, 
    [ currentPage ]);

    const updateTotal = (result:any) => 
    {
        // RODA NO INICIO
        if (!totalPage)
        {
            setHasNextPage(currentPage < result.totalPage);
            setHasPrevPage(currentPage > 1);
			
            MapPages.pages[currentPage] = null;
        }
		
        setTotalPage(result.totalPage);
        setTotal(result.total);
    }

    const updateCollection = (result:any) => 
    {
        if (result?.collection?.length > 0)
        {
            MapPages.pages[currentPage + 1] = {
                referencePath : result?.collection[result?.collection?.length - 1]?.referencePath
            };
        }
    }

    const nextPage = () => 
    {
        if (currentPage < totalPage)
        {
            setCurrentPage(currentPage + 1);			
        }		
    }

    const prevPage = () => 
    {
        if (currentPage > 0)
        {
            setCurrentPage(currentPage - 1);
        }
    }

    return { 
        currentPage, 
        totalPage, 
        nextPage, 
        prevPage, 
        updateTotal, 
        updateCollection, 
        total, 
        hasNextPage, 
        hasPrevPage 
    }
};

export { Pagination }