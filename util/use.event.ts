import { usePathname } from "next/navigation";
import { useEffect } from "react";
class UseEvents 
{
    static events    : any = {};
    static dispatchs : any = {};
}

const useClearEvent = () => 
{
    const pathname = usePathname();

    useEffect(() => 
    {
        //console.log("clearEvent");
        
        UseEvents.events    = {};
        UseEvents.dispatchs = {};
    }, [ pathname ]);
};

const dispatchEvent = (key: any, data?: any) => 
{
    UseEvents.dispatchs[key] = data;

    if (UseEvents.events[key]) 
    {
        for (const callback of UseEvents.events[key]) 
        {
            //console.log("dispatchEvent", key, data, UseEvents.events[key].length);
          
            callback(data);
        }
    }
};

const useOnEvent = (key: any, callback: any) => 
{
    const pathname = usePathname();

    useEffect(() => 
    {
        //console.error('useOnEvent', key)

        if (!UseEvents.events[key]) 
        {
            UseEvents.events[key] = [];
        }

        UseEvents.events[key].push(callback);

        if (UseEvents.dispatchs[key]) 
        {
            callback(UseEvents.dispatchs[key]);
        } 
      
    }, [ pathname ]);
};

export { 
    useClearEvent, 
    dispatchEvent, 
    useOnEvent 
};
