import { NextApiRequest, NextApiResponse } from "next";

declare const fetch:any;

export default async function handlerProxy(req: NextApiRequest, res: NextApiResponse) 
{
    try 
    {
        const { body, query, headers } : any = req;

        if (query.path)
        {
            const paths : any = query.path;
            const path = paths.join("/")

            const apiGatewayUrl = process.env.NEXT_PUBLIC_API_GATEWAY_URL || "";
            const apiKey        = process.env.NEXT_PUBLIC_API_GATEWAY_KEY;

            headers["x-api-key"] = apiKey;

            const response = await fetch(apiGatewayUrl + "/" + path, {
                method      : "POST",
                body        : JSON.stringify(body),
                headers     : headers,
                credentials : "include"
            });

            if (!response.ok) 
            {
                return res.status(response.status).json({ message : "Erro ao acessar API Gateway" });
            }

            const data = await response.json();

            res.status(response.status).json(data);
        }
        else
        {
            res.status(500).json({ message : "Erro ao acessar API Gateway" });
        }        
    } 
    catch (e:any) 
    {
        e;
        res.status(500).json({ message : "Erro ao acessar API Gateway" });
    }
}